from flask import Flask, render_template, redirect, request
from flask_debugtoolbar import DebugToolbarExtension
from forms import MyForm1, MyForm2, PartialForm
app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'abc123'
toolbar = DebugToolbarExtension(app)

class Guy(object):
    def __init__(self, name, job, years):
        self.name = name
        self.job  = job
        self.years = years
        
    def __repr__(self):
        return "%s worked in %s for %s years" % (self.name, self.job, self.years)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/form1', methods=('GET', 'POST'))
def form1():
    guy = Guy(name="Nick", job="IT", years=5)
    form = MyForm1()
    if form.validate_on_submit():
        form.populate_obj(guy)
        return guy.__repr__()
    return render_template('form1.html', form=form, guy=guy)

@app.route('/form2', methods=('GET', 'POST'))
def form2():
    guy = Guy(name="Nick", job="IT", years=5)
    form = MyForm2()
    if form.validate_on_submit():
        form.populate_obj(guy)
        return guy.__repr__()
    return render_template('form2.html', form=form, guy=guy)

@app.route('/partial', methods=('GET', 'POST'))
def partial():
    guy = Guy(name="Nick", job="IT", years=5)
    form = PartialForm(obj=guy)

    if form.validate_on_submit():
        form.populate_obj(guy, dict(request.form))
        return guy.__repr__()
    return render_template('partial.html', form=form, guy=guy)

if __name__ == "__main__":
    app.run()
