from flask_wtf import Form
from wtforms.validators import Required, Optional

class ComposableForm(Form):

    def populate_obj_partial(self, obj, update_dict):
        """ Update object fields only if contained in update_dict """
        for field in self:
            if field.name in update_dict and hasattr(obj, field.name):
                setattr(obj, field.name, getattr(self, field.name).data)

    def populate_obj(self, obj, update_dict=None):
        """ Call our partial method or standard method """
        if update_dict:
            self.populate_obj_partial(obj, update_dict)
        else:
            super(ComposableForm, self).populate_obj(obj)      

    def required_fields(self, field_list):
        """ Set fields in fields list to required, set a new is_required attribute """
        for field in field_list:
            new_validators = []
            for validator in getattr(self, field).validators:
                if not isinstance(validator, Optional) and not isinstance(validator, Required):
                    new_validators.append(validator)
            new_validators.append(Required())                     
            getattr(self, field).validators = new_validators
            getattr(self, field).is_required = True

    def optional_fields(self, field_list):
        """ Set fields in fields list to optional, set a new is_required attribute """
        for field in field_list:
            new_validators = []
            for validator in getattr(self, field).validators:
                if not isinstance(validator, Optional) and not isinstance(validator, Required):
                    new_validators.append(validator)
            new_validators.append(Optional())                    
            getattr(self, field).validators = new_validators
            getattr(self, field).is_required = False

