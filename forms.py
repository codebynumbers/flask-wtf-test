from composable_form import ComposableForm
from wtforms import TextField, IntegerField
from wtforms.validators import DataRequired, Length, Optional, NumberRange
from copy import deepcopy

""" Here we define the fields and type validation, once and only once """
name_field = TextField('Name', validators=[Length(min=3, max=10)])
job_field = TextField('Job Title', validators=[])
years_field = IntegerField('Number of years', validators=[NumberRange(min=1, max=10)])

class MyForm1(ComposableForm):
    """ Here we define a form that collects these fields as required """

    name  = deepcopy(name_field)
    job   = deepcopy(job_field)

    def __init__(self, *args, **kwargs):
        super(MyForm1, self).__init__(*args, **kwargs)
        self.required_fields(['name', 'job'])

class MyForm2(ComposableForm):
    """ Here we define another form that collects a different set of these fields """

    name  = deepcopy(name_field)
    job   = deepcopy(job_field)
    years = deepcopy(years_field)

    def __init__(self, *args, **kwargs):
        super(MyForm2, self).__init__(*args, **kwargs)
        self.required_fields(['name', 'years'])
        self.optional_fields(['job'])

class PartialForm(ComposableForm):
    """ Here we define another form that allows update of any single field without clearing object 
        In other words only object object with data we receive, not entrire form
        This allows you to make updates on specific attributes
    """
    name  = deepcopy(name_field)
    job   = deepcopy(job_field)
    years = deepcopy(years_field)

    def __init__(self, *args, **kwargs):
        super(PartialForm, self).__init__(*args, **kwargs)
        self.optional_fields(['name', 'job', 'years'])

